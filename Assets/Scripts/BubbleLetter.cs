﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BubbleLetter : MonoBehaviour {

	public Text letterText;
	public Image morseImage;
	public Color normalColor = Color.black;
	public Color correctColor = Color.green;
	public ParticleSystem particle;
	public void SetLetter(char letter, Sprite morse){
		letterText.text = letter.ToString();
		morseImage.sprite = morse;
	}

	public void SetLetterCorrect(){
		letterText.color = correctColor;
		particle.Play();
	}
}
