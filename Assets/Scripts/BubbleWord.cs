﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BubbleWord : MonoBehaviour {

	public RectTransform letterParent;
	public BubbleLetter letterReference;

	string currentWord;

	List<BubbleLetter> letterList;
	Dictionary<char, Sprite> morseList;

	void Awake(){
		letterList = new List<BubbleLetter>();
		morseList = new Dictionary<char, Sprite>();

		morseList.Add('A', Resources.Load<Sprite>("morse/A"));
		morseList.Add('B', Resources.Load<Sprite>("morse/B"));
		morseList.Add('C', Resources.Load<Sprite>("morse/C"));
		morseList.Add('D', Resources.Load<Sprite>("morse/D"));
		morseList.Add('E', Resources.Load<Sprite>("morse/E"));
		morseList.Add('F', Resources.Load<Sprite>("morse/F"));
		morseList.Add('G', Resources.Load<Sprite>("morse/G"));
		morseList.Add('H', Resources.Load<Sprite>("morse/H"));
		morseList.Add('I', Resources.Load<Sprite>("morse/I"));
		morseList.Add('J', Resources.Load<Sprite>("morse/J"));
		morseList.Add('K', Resources.Load<Sprite>("morse/K"));
		morseList.Add('L', Resources.Load<Sprite>("morse/L"));
		morseList.Add('M', Resources.Load<Sprite>("morse/M"));
		morseList.Add('N', Resources.Load<Sprite>("morse/N"));
		morseList.Add('O', Resources.Load<Sprite>("morse/O"));
		morseList.Add('P', Resources.Load<Sprite>("morse/P"));
		morseList.Add('Q', Resources.Load<Sprite>("morse/Q"));
		morseList.Add('R', Resources.Load<Sprite>("morse/R"));
		morseList.Add('S', Resources.Load<Sprite>("morse/S"));
		morseList.Add('T', Resources.Load<Sprite>("morse/T"));
		morseList.Add('U', Resources.Load<Sprite>("morse/U"));
		morseList.Add('V', Resources.Load<Sprite>("morse/V"));
		morseList.Add('W', Resources.Load<Sprite>("morse/W"));
		morseList.Add('X', Resources.Load<Sprite>("morse/X"));
		morseList.Add('Y', Resources.Load<Sprite>("morse/Y"));
		morseList.Add('Z', Resources.Load<Sprite>("morse/Z"));
	}

	void Start(){
		letterReference.gameObject.SetActive(false);
	}
	public void SetLetterCorrect(int letterIndex){
		letterList[letterIndex].SetLetterCorrect();
	}
	public void SetWord(string word){

		foreach (var item in letterList) {
			Destroy(item.gameObject);
		}

		letterList.Clear();
		letterReference.gameObject.SetActive(true);
		currentWord = word;
		char[] lettersFromWord = currentWord.ToCharArray();
		for (int i = 0; i < lettersFromWord.Length; i++) {

			char letter = lettersFromWord[i];

			BubbleLetter newLetter = Instantiate(letterReference) as BubbleLetter;
			newLetter.transform.SetParent(letterParent);
			newLetter.transform.localScale = Vector3.one;
			newLetter.SetLetter(letter, morseList[letter]);

			letterList.Add(newLetter);
		}

		letterReference.gameObject.SetActive(false);
	}
}
