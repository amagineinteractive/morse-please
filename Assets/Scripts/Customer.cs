﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Customer : MonoBehaviour {

	public Canvas customerCanvas;
	public Image customerImage;
	public Image customerTint;
	public Image activeChannel;
	public Image inactiveChannel;
	public Text inactiveText;

	public Sprite[] ActiveChannelSprites;
	public Sprite[] InactiveChannelSprites;
	CustomerData data;

	bool isActive;

	public CustomerData Data {
		get {
			return data;
		}
	}

	public void Initialize(CustomerData data){
		this.data = data;
		this.customerImage.sprite = data.normalSprite;
		this.inactiveText.text = data.word;
		this.activeChannel.sprite = ActiveChannelSprites[data.channel-1];
		this.inactiveChannel.sprite = InactiveChannelSprites[data.channel-1];
		SetActiveCustomer(false);
	}

	public void SetFace(bool isAngry){
		this.customerImage.sprite = isAngry? data.angrySprite : data.normalSprite;
	}

	public void SetActiveCustomer(bool active){
		customerTint.gameObject.SetActive(!active);
		activeChannel.gameObject.SetActive(active);
		inactiveChannel.gameObject.SetActive(!active);
		customerCanvas.sortingOrder = active? 3 : 1;
	}

}
