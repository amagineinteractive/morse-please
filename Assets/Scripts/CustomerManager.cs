﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomerManager : MonoBehaviour {

	public int channelCount = 5;
	public TextAsset[] wordListText;
	public string[] wordList;
	public Sprite[] charSprites;
	public Sprite[] angrySprite;

	public Customer customerReference;
	public CustomerQueue customerQueue;

	void Awake(){
		TextAsset selected = wordListText[Random.Range(0, wordListText.Length)];

		wordList = selected.text.Split('\n');

	}
	void Start(){
		customerReference.gameObject.SetActive(false);
	}

	public Customer GenerateRandomCustomer(){

		CustomerData data = new CustomerData();
		int channel = 0;
		do{
			channel = Random.Range(0, channelCount) +1;
		}while(HasCustomerWithChannel(channel));

		data.channel = channel;
		int spriteIndex = Random.Range(0, charSprites.Length);
		data.normalSprite = charSprites[spriteIndex];
		data.angrySprite = angrySprite[spriteIndex];
		data.word = wordList[Random.Range(0, wordList.Length)];

		customerReference.gameObject.SetActive(true);
		Customer newCust = Instantiate(customerReference) as Customer;
		newCust.Initialize(data);
		customerQueue.AddToQueue(newCust);
		customerReference.gameObject.SetActive(false);
	
		return newCust;
	}

	public Customer GetCustomerWithChannel(int channel){
		return customerQueue.CustomerList.Find( x=> x.Data.channel == channel);
	}

	public bool HasCustomerWithChannel(int channel){
		return customerQueue.CustomerList.Exists( x=> x.Data.channel == channel);
	}
	public void SwitchCustomer(Customer customer){
		if(customer != customerQueue.CustomerList[0]){
			customerQueue.SwitchPosition(customer, customerQueue.CustomerList[0]);
		}
	}

	public void RemoveCustomer(Customer oldCustomer){
		customerQueue.RemoveCustomer(oldCustomer);
	}

	public Customer GetActiveCustomer(){
		return customerQueue.CustomerList[0];
	}

	public bool CheckIfActiveCustomer(Customer customer){
		return customerQueue.CustomerList.IndexOf(customer) == 0;
	}

	public bool CanGenerateCustomer(){
		return customerQueue.CanAddToQueue;
	}
//
//
//	void Update () {
//		if(Input.GetKeyDown(KeyCode.A)){
//			if(customerQueue.CanAddToQueue){
//				GenerateRandomCustomer();
//			}
//		}
//	}
}

public class CustomerData{
	public string word;
	public int channel;
	public Sprite angrySprite;
	public Sprite normalSprite;
}