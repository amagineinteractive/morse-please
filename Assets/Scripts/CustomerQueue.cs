﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomerQueue : MonoBehaviour {

	public Transform QueueRoot;

	public Transform[] queuePositions;

	List<Customer> customerList;

	public bool CanAddToQueue{
		get{
			return customerList.Count < queuePositions.Length;
		}
	}

	public List<Customer> CustomerList {
		get {
			return customerList;
		}
	}

	void Awake(){
		customerList = new List<Customer>();
	}

	public void AddToQueue(Customer customer){
		if(CanAddToQueue){
			customerList.Add(customer);
			int customerIndex = customerList.IndexOf(customer);

			customer.transform.SetParent(QueueRoot);
			customer.transform.localScale = Vector3.one;
			customer.transform.position = queuePositions[customerIndex].position;
		}

		OrganizeActiveCustomer();
	}

	public void RemoveCustomer(Customer customer){
		customerList.Remove(customer);
		Destroy(customer.gameObject);

		for (int i = 0; i < customerList.Count; i++) {
			customerList[i].transform.position = queuePositions[i].position;
		}

		OrganizeActiveCustomer();
	}
	void OrganizeActiveCustomer(){
		foreach (var item in customerList) {
			item.SetActiveCustomer(false);
		}
		customerList[0].SetActiveCustomer(true);
	}

	public void SwitchPosition(Customer customerA, Customer customerB){
		int indexA = customerList.IndexOf(customerA);
		int indexB = customerList.IndexOf(customerB);

		customerList[indexA] = customerB;
		customerList[indexB] = customerA;

		customerA.transform.position = queuePositions[indexB].position;
		customerB.transform.position = queuePositions[indexA].position;

		OrganizeActiveCustomer();
	}
}
