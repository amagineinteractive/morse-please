﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class GameManager : MonoBehaviour {

	public WordProcessor wordProcessor;
	public CustomerManager customerManager;
	public float generateInterval = 2;

	[Header("score and Time")]
	public Text scoreText;
	public Text timeText;
	public Image timerFillBar;

	[Header("start")]
	public CanvasGroup startMenu;
	public RectTransform startLogo;
	public Image startChar;
	public Button startButton;

	[Header("result")]
	public Image overlay;
	public CanvasGroup creditView;
	public CanvasGroup resultView;
	public Text resultScoreText;
	public Text resultHighScoreText;
	public Text resultMessageText;
	public Text resultTopMessageText;

	Customer currentCustomer;

	public float gameTime;
	public string replayScene;

	int score;
	int messageSent;

	float timer;

	bool gameRun = false;
	void OnEnable(){
		wordProcessor.onWordComplete += HandleOnWordComplete;
		wordProcessor.onCorrectLetter += HandleOnCorrectLetter;
		wordProcessor.onWrongLetter += HandleOnWrongLetter;
	}


	void OnDisable(){
		wordProcessor.onWordComplete -= HandleOnWordComplete;
		wordProcessor.onCorrectLetter -= HandleOnCorrectLetter;
		wordProcessor.onWrongLetter -= HandleOnWrongLetter;
	}

	void HandleOnCorrectLetter ()
	{
		score += 100;
		scoreText.text = score.ToString();
		currentCustomer.SetFace(false);
		SoundHelper.Instance.PlaySfx("benar", 1);
	}

	void HandleOnWrongLetter ()
	{
		currentCustomer.SetFace(true);
		SoundHelper.Instance.PlaySfx("salah", 1);
	}

	void HandleOnWordComplete ()
	{
		customerManager.RemoveCustomer(currentCustomer);
		currentCustomer = customerManager.GetActiveCustomer();
		wordProcessor.SetWord(currentCustomer.Data.word);
		messageSent ++;
	}

	// Use this for initialization
	void Start () {
		overlay.gameObject.SetActive(false);
		creditView.gameObject.SetActive(false);
		resultView.gameObject.SetActive(false);
		timer = gameTime;

		score = 0;
		messageSent = 0;
		scoreText.text = score.ToString();
		wordProcessor.DisableMorse();
		UpdateTimeView();

		gameRun = false;

		AnimateStart();
	}

	void AnimateStart(){
		startMenu.gameObject.SetActive(true);

		Sequence seq = DOTween.Sequence();

		startMenu.interactable = false;

		seq.AppendInterval(0.5f);
		seq.Append(startLogo.DOAnchorPosY(1200, 0.5f).From().SetEase(Ease.OutBack));
		seq.Append(startChar.DOFade(0,0.5f).From());
		seq.Join(startButton.transform.DOScale(0, 0.5f).From().SetEase(Ease.OutBack));
		seq.AppendCallback(()=>{
			startMenu.interactable = true;
		});
	}
	public void StartGame(){

		startMenu.interactable = false;

		startMenu.DOFade(0, 0.5f).OnComplete(()=>{
			startMenu.gameObject.SetActive(false);
			gameRun = true;
			StartCoroutine(GenerateCustomer(generateInterval));
		});
	}
	void Update(){
		if(gameRun){
			timer -= Time.deltaTime;
			if(timer < 0){
				timer = 0;
				GameOver();
			}
			UpdateTimeView();
		}
	}

	void UpdateTimeView(){
		timerFillBar.fillAmount = timer/gameTime;
		timeText.text = FormatTime(timer);
	}

	string FormatTime(float time){
		int second = Mathf.CeilToInt(time % 60);
		int minute = Mathf.FloorToInt(time / 60);

		return minute.ToString("00")+":"+second.ToString("00");
	}
	void GameOver(){
		gameRun = false;
		wordProcessor.DisableMorse();

		int highscore = PlayerPrefs.GetInt("scoreHighscore", 0);
		int topMessage = PlayerPrefs.GetInt("messageHighscore", 0);

		if(score > highscore){
			highscore = score;
			PlayerPrefs.SetInt("scoreHighscore", score);
		}
		if(messageSent > topMessage){
			topMessage = messageSent;
			PlayerPrefs.SetInt("messageHighscore", messageSent);
		}
			
		resultScoreText.text = score.ToString();
		resultMessageText.text = messageSent.ToString();
		resultHighScoreText.text = highscore.ToString();
		resultTopMessageText.text = topMessage.ToString();

		overlay.gameObject.SetActive(true);
		resultView.gameObject.SetActive(true);

		Sequence seq = DOTween.Sequence();

		resultView.interactable = false;
		seq.Append(overlay.DOFade(0, 0.3f).From());
		seq.Join(resultView.GetComponent<RectTransform>().DOAnchorPosY(1200, 0.5f).From().SetEase(Ease.OutBack));
		seq.Join(resultView.DOFade(0,0.5f).From());
		seq.AppendCallback(()=>{
			resultView.interactable = true;
		});

	}

	public void ShowCredit(){

		Sequence seq = DOTween.Sequence();

		Vector3 origPos = resultView.transform.position;

		seq.AppendCallback(()=>{
			creditView.gameObject.SetActive(true);
			creditView.interactable = false;
			resultView.interactable = false;
		});
		seq.Append(creditView.GetComponent<RectTransform>().DOAnchorPosX(-1000, 0.5f).From().SetEase(Ease.OutBack));
		seq.Join(creditView.DOFade(0,0.5f).From());
		seq.Join(resultView.GetComponent<RectTransform>().DOAnchorPosX(1000, 0.5f).SetEase(Ease.OutBack));
		seq.Join(resultView.DOFade(0,0.5f));
		seq.AppendCallback(()=>{
			creditView.interactable = true;
			resultView.interactable = true;
			creditView.alpha = 1;
			resultView.alpha = 1;
			resultView.transform.position = origPos;
			resultView.gameObject.SetActive(false);
		});
	}

	public void CloseCredit(){

		Sequence seq = DOTween.Sequence();

		Vector3 origPos = creditView.transform.position;

		seq.AppendCallback(()=>{
			resultView.gameObject.SetActive(true);
			creditView.interactable = false;
			resultView.interactable = false;
		});
		seq.Append(creditView.GetComponent<RectTransform>().DOAnchorPosX(-1000, 0.5f).SetEase(Ease.OutBack));
		seq.Join(creditView.DOFade(0, 0.5f));
		seq.Join(resultView.GetComponent<RectTransform>().DOAnchorPosX(1000, 0.5f).From().SetEase(Ease.OutBack));
		seq.Join(resultView.DOFade(0, 0.5f).From());
		seq.AppendCallback(()=>{
			creditView.interactable = true;
			resultView.interactable = true;
			creditView.alpha = 1;
			resultView.alpha = 1;
			creditView.gameObject.SetActive(false);
			creditView.transform.position = origPos;
		});
	}

	public void RestartGame(){
		SceneManager.LoadScene(replayScene);
	}
	public void SelectChannel(int channel){
		if(customerManager.HasCustomerWithChannel(channel)){
			Customer newCustomer = customerManager.GetCustomerWithChannel(channel);
			if(newCustomer != currentCustomer){
				customerManager.SwitchCustomer(newCustomer);
				MainCustomerSwitched(customerManager.GetActiveCustomer());
			}
		}
	}

	void OnCustomerGenerated(Customer customer){
		if(customerManager.CheckIfActiveCustomer(customer)){
			currentCustomer = customer;
			wordProcessor.EnableMorse();
			wordProcessor.SetWord(customer.Data.word);
		}
	}

	public void MainCustomerSwitched(Customer customer){
		currentCustomer = customer;
		wordProcessor.SetWord(customer.Data.word);
	}

	IEnumerator GenerateCustomer(float timeInterval){

		while(true){
			if(customerManager.CanGenerateCustomer()){
				Customer customer = customerManager.GenerateRandomCustomer();
				OnCustomerGenerated(customer);
			}

			yield return new WaitForSeconds(timeInterval);
		}
//		GenerateCustomer(timeInterval);
	}
}
