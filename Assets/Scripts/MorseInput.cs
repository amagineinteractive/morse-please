﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum MORSE_CODE{
	SHORT = '.',
	LONG = '_'
}

public class MorseInput : MonoBehaviour {

	public delegate void OnCodePressed(MORSE_CODE code);
	public delegate void OnLetterGenerated(char letter);

	public OnCodePressed onCodePressed;
	public OnLetterGenerated onLetterGenerated;
	public float longPressTime = .15f;
	public float spaceDuration = .5f; 
	public bool useNumeric = false;
	int maxCodeLength = 4;
	bool pressed = false;
	float timer = 0;

	float codeTimer = 0;
	List<MORSE_CODE> currentCodes;

	Dictionary<MORSE_CODE, char> codeDict;
	
	Dictionary<string, char> morseLetter;

	AudioSource audioSource;

	public bool canInput = true;
	void Awake(){

		audioSource = GetComponent<AudioSource>();

		currentCodes = new List<MORSE_CODE>();
		codeDict = new Dictionary<MORSE_CODE, char>();
		codeDict.Add(MORSE_CODE.SHORT, '.');
		codeDict.Add(MORSE_CODE.LONG, '-');

		morseLetter = new Dictionary<string, char>();
		morseLetter.Add(".-"	, 'A');
		morseLetter.Add("-..."	, 'B');
		morseLetter.Add("-.-."	, 'C');
		morseLetter.Add("-.."	, 'D');
		morseLetter.Add("."		, 'E');
		morseLetter.Add("..-."	, 'F');
		morseLetter.Add("--."	, 'G');
		morseLetter.Add("...."	, 'H');
		morseLetter.Add(".."	, 'I');
		morseLetter.Add(".---"	, 'J');
		morseLetter.Add("-.-"	, 'K');
		morseLetter.Add(".-.."	, 'L');
		morseLetter.Add("--"	, 'M');
		morseLetter.Add("-."	, 'N');
		morseLetter.Add("---"	, 'O');
		morseLetter.Add(".--."	, 'P');
		morseLetter.Add("--.-"	, 'Q');
		morseLetter.Add(".-."	, 'R');
		morseLetter.Add("..."	, 'S');
		morseLetter.Add("-"		, 'T');
		morseLetter.Add("..-"	, 'U');
		morseLetter.Add("...-"	, 'V');
		morseLetter.Add(".--"	, 'W');
		morseLetter.Add("-..-"	, 'X');
		morseLetter.Add("-.--"	, 'Y');
		morseLetter.Add("--.."	, 'Z');
		morseLetter.Add(".----"	, '1');
		morseLetter.Add("..---"	, '2');
		morseLetter.Add("...--"	, '3');
		morseLetter.Add("....-"	, '4');
		morseLetter.Add("....."	, '5');
		morseLetter.Add("-...."	, '6');
		morseLetter.Add("--..."	, '7');
		morseLetter.Add("---.."	, '8');
		morseLetter.Add("----."	, '9');
		morseLetter.Add("-----"	, '0');


		maxCodeLength = useNumeric? 5:4;

	}

	public char EnumToCode(MORSE_CODE code){
		return codeDict[code];
	}

	public void Clear(){
		currentCodes.Clear();
	}
	public void PressDown(){
		if(!pressed) {
			pressed = true;
			timer = 0;
			audioSource.Play();
		}
	}

	public void PressUp(){
		if(pressed){
			CheckMorseInput();
			pressed = false;
			timer = 0;
			codeTimer = 0;
			audioSource.Stop();
		}
	}

	void ProcessKeyInput(){
		if(Input.GetKeyDown(KeyCode.Space)){	
			PressDown();
		}

		if(Input.GetKeyUp(KeyCode.Space)){
			PressUp();
		}
	}
	// Update is called once per frame
	void Update () {
		if(canInput){
			ProcessKeyInput();
		}

		if(pressed){
			timer +=Time.deltaTime;
		}
		else if(!pressed && currentCodes.Count > 0) {
			codeTimer += Time.deltaTime;
			if(codeTimer > spaceDuration){
				LetterGenerated();
			}
		}
	}

	void LetterGenerated(){
		if(onLetterGenerated != null) onLetterGenerated(GetCharFromMorse(currentCodes));
		currentCodes.Clear();
	}

	char GetCharFromMorse(List<MORSE_CODE> codeList){
		string print = "";
		foreach (var item in codeList) {
			print += codeDict[item];
		}
		if(morseLetter.ContainsKey(print))
			return morseLetter[print];
		else return '\0';
	}

	void CheckMorseInput(){
		if(timer > longPressTime){
			ProcessCode(MORSE_CODE.LONG);
		}
		else{
			ProcessCode(MORSE_CODE.SHORT);
		}
	}

	void ProcessCode(MORSE_CODE code){
		currentCodes.Add(code);
		if(onCodePressed != null) onCodePressed(code);

		if(currentCodes.Count >=maxCodeLength){
			LetterGenerated();
		}
	}

}
