﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class MorseView : MonoBehaviour {

	public Text letterText;
	public RectTransform viewParent;
	public Image wrongImage;
	public Image correctImage;
	public List<Image> dotList;
	public List<Image> dashList;

	List<Image> shownList;
	int index;

	void Awake(){
		shownList = new List<Image>();

		foreach (var dot in dotList) {
			dot.gameObject.SetActive(false);	
		}

		foreach (var dash in dashList) {
			dash.gameObject.SetActive(false);
		}

		letterText.gameObject.SetActive(false);
	}
	public void AddCode(MORSE_CODE code){
		List<Image> toShowList = code == MORSE_CODE.LONG? dashList : dotList;

		Image codeToShow = toShowList.Find( x => !x.gameObject.activeSelf);

		codeToShow.gameObject.SetActive(true);
		codeToShow.transform.SetSiblingIndex(index);
		shownList.Add(codeToShow);

		index++;
		codeToShow.transform.DOScale(0, 0.3f).From().SetEase(Ease.OutBack);
	}

	public void AnimateComplete(char letter, bool correct, TweenCallback onComplete){
		letterText.text = letter.ToString();
		letterText.gameObject.SetActive(true);

		Sequence seq = DOTween.Sequence();
		if(shownList.Count >= 4){
			seq.AppendInterval(0.3f);
		}
		seq.AppendCallback(()=>{
			if(correct)
				correctImage.gameObject.SetActive(true);
			else
				wrongImage.gameObject.SetActive(true);
		});
		seq.Append(viewParent.DOScale(0, 0.3f).SetEase(Ease.OutQuad));
		seq.Join(letterText.rectTransform.DOScale(0, 0.3f).From().SetEase(Ease.OutBack));
		if(correct){
			seq.Join(correctImage.DOFade(0,0.25f).OnComplete(()=>{
				correctImage.color = Color.white;
				correctImage.gameObject.SetActive(false);
			}));
		}
		else{
			seq.Join(wrongImage.DOFade(0,0.25f).OnComplete(()=>{
				wrongImage.color = Color.white;
				wrongImage.gameObject.SetActive(false);
			}));
		}
//		seq.AppendInterval(0.1f);
		seq.AppendCallback(()=>{
			letterText.gameObject.SetActive(false);
			letterText.rectTransform.localScale = Vector3.one;
			viewParent.localScale = Vector3.one;
			onComplete();
		});
	}

	public void ClearView(){
		foreach (var item in shownList) {
			item.gameObject.SetActive(false);
		}
		shownList.Clear();
	}
}
