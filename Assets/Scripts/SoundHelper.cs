﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoundHelper : MonoBehaviour {

	public static SoundHelper Instance;

	public SoundEffect[] SoundEffects;
	public static AudioSource ac;

	Dictionary<string, AudioClip> clipDict;

	void Awake () {
		if (Instance !=null && Instance != this){
			Destroy(this.gameObject);
			return;
		}else {
			Instance = this;
			ac = GetComponent<AudioSource>();

			clipDict = new Dictionary<string, AudioClip>();

			foreach (var sfx in SoundEffects) {
				clipDict.Add(sfx.name, sfx.clip);
			}
		}
		DontDestroyOnLoad(this);
	}

	public void PlaySfx(string key, float volume){
		ac.PlayOneShot(clipDict[key],volume);
	}

	
}

[System.Serializable]
public class SoundEffect{

	public AudioClip clip;
	public string name;
}