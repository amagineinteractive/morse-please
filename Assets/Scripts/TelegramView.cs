﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class TelegramView : MonoBehaviour {

	public Sprite normalSprite;
	public Sprite pressedSprite;
	public MorseInput morse;
	Image telegramImage;

	bool canClick = true;
	public bool CanClick{
		get{
			return canClick;
		}
		set{
			canClick = value;
			morse.canInput = canClick;
		}
	}

	void Awake(){
		telegramImage = GetComponent<Image>();
		telegramImage.sprite = normalSprite;
	}

	public void OnTelegramPressed(){
		if(CanClick){
			telegramImage.sprite = pressedSprite;
			morse.PressDown();
		}
	}

	public void OnTelegramReleased(){
		if(CanClick){
			telegramImage.sprite = normalSprite;
			morse.PressUp();
		}
	}
}
