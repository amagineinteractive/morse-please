﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class WordProcessor : MonoBehaviour {

	public delegate void OnCorrectLetter();
	public delegate void OnWrongLetter();
	public delegate void OnWordComplete();

	public OnCorrectLetter onCorrectLetter;
	public OnWrongLetter onWrongLetter;
	public OnWordComplete onWordComplete;
	string currentWord;
	char[] letterList;
	int currIndex;

	public BubbleWord bubbleWord;
//	public Text currWordText;
	public Text outputText;
	public MorseInput morseInput;
	public MorseView morseView;
	public TelegramView telegram;

	bool canProcess;
	public void DisableMorse(){
		canProcess = false;
	}

	public void EnableMorse(){
		canProcess = true;
	}
	void OnEnable(){
		morseInput.onCodePressed += OnCodePressed;
		morseInput.onLetterGenerated += OnLetterGenerated;
	}

	void OnDisable(){
		morseInput.onCodePressed -= OnCodePressed;
		morseInput.onLetterGenerated -= OnLetterGenerated;
		
	}

	void Awake(){
		canProcess = true;
	}

	void Start(){
		outputText.text ="";
	}
	public void SetWord(string word){
		
		currentWord = word.ToUpper();
		letterList = currentWord.ToCharArray();
		currIndex = 0;
//		currWordText.text = currentWord;
		bubbleWord.SetWord(currentWord);
		outputText.text = "";
		morseView.ClearView();

	}

	void WordComplete(){
		telegram.CanClick = false;
		DOVirtual.DelayedCall(0.5f,
			()=>{
				telegram.CanClick = canProcess;
				if(onWordComplete != null) onWordComplete();
			});
	}

	void OnCodePressed(MORSE_CODE code){ 
//		codeText.text += morseInput.EnumToCode(code);
		if(canProcess)
			morseView.AddCode(code);
	}

	void OnLetterGenerated(char letter){
//		Debug.Log("letter:"+letter+" ,list[i]:"+letterList[currIndex]);
		if(!canProcess) return;

		telegram.CanClick = false;
		bool correctLetter = letter == letterList[currIndex];

		morseView.AnimateComplete(letter, correctLetter, ()=>{
			telegram.CanClick = canProcess;

			if(correctLetter){
				if(onCorrectLetter != null)
					onCorrectLetter();
				
				outputText.text += letter;
				bubbleWord.SetLetterCorrect(currIndex);
				currIndex++;
				if(currIndex >= letterList.Length){
					WordComplete();
				}
			}
			else{
				if(onWrongLetter != null){
					onWrongLetter();
				}
			}

			morseView.ClearView();

		});
	}
}
